<?php

// Include the default settings.
if (file_exists(DRUPAL_ROOT . '/sites/default/settings.default.php')) {
  require DRUPAL_ROOT . '/sites/default/settings.default.php';
}

// Database.
$ddev_default_database = [
  'driver' => 'mysql',
  'host' => 'db',
  'port' => '3306',
  'database' => 'db',
  'username' => 'db',
  'password' => 'db',
  'prefix' => '',
];

// All database drivers have their own module from Drupal 9.4.
if (is_dir(DRUPAL_ROOT . '/core/modules/mysql')) {
  $ddev_default_database += [
    'namespace' => 'Drupal\\mysql\\Driver\\Database\\mysql',
    'autoload' => 'core/modules/mysql/src/Driver/Database/mysql/',
  ];
}

$databases['default']['default'] = $ddev_default_database;

// Salt for one-time login links, cancel links, form tokens, etc.
$settings['hash_salt'] = 'abcdefghijklmnopqrstuvwxyz0123456789';

// File paths.
$settings['file_public_path'] = 'sites/default/files';
$settings['file_private_path'] = 'sites/default/files/private';
$settings['file_temp_path'] = 'sites/default/files/tmp';

// Skip hardening the folder permissions.
$settings['skip_permissions_hardening'] = TRUE;

// Folders excluded from scanning.
$settings['file_scan_ignore_directories'] = [
  'node_modules',
  'bower_components',
];

// Trusted hosts.
$settings['trusted_host_patterns'][] = '\.ddev\.site$';

// Reverse proxy.
$settings['reverse_proxy'] = TRUE;
$settings['reverse_proxy_addresses'] = [];

// Add adresses specified in the forwarded for header, except the real IP.
if (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
  $ips = explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']);
  $ips = array_map('trim', $ips);

  if (!empty($_SERVER['HTTP_X_REAL_IP'])) {
    $ips = array_diff($ips, [$_SERVER['HTTP_X_REAL_IP']]);
  }

  $settings['reverse_proxy_addresses'] = array_merge(
    $settings['reverse_proxy_addresses'],
    $ips
  );
}

if (!empty($_SERVER['REMOTE_ADDR'])) {
  // The remote address can never be the client as Traefik is at least in between.
  $settings['reverse_proxy_addresses'][] = $_SERVER['REMOTE_ADDR'];
}

// Memcache.
$settings['memcache']['servers'] = ['memcached:11211' => 'default'];

// Redis.
$settings['redis.connection']['interface'] = 'PhpRedis';
$settings['redis.connection']['host'] = 'redis';
$settings['redis.connection']['port'] = '6379';

// Load the development services.
if (file_exists($app_root . '/sites/development.services.yml')) {
  $settings['container_yamls'][] = $app_root . '/sites/development.services.yml';
}

// Include the local settings.
if (file_exists(DRUPAL_ROOT . '/../.dropsolid/scaffold/local/[[DDP_DOCROOT]]/sites/default/settings.php')) {
  require DRUPAL_ROOT . '/../.dropsolid/scaffold/local/[[DDP_DOCROOT]]/sites/default/settings.php';
}
elseif (file_exists(DRUPAL_ROOT . '/../etc/drupal/settings_local.php')) {
  require DRUPAL_ROOT . '/../etc/drupal/settings_local.php';
}

// If the local settings file replaced the database credentials with the ones
// from Launchpad we need to revert that change again.
if (($databases['default']['default']['host'] ?? NULL) === 'localhost' && !empty($ddev_default_database)) {
  $databases['default']['default'] = $ddev_default_database;
}

// Try to set the Stage File Proxy origin.
if (defined('ROCKETSHIP_PROJECT_NAME') && empty($config['stage_file_proxy.settings']['origin'])) {
  $config['stage_file_proxy.settings']['origin'] = 'https://' . ROCKETSHIP_PROJECT_NAME . '.staging.sites.dropsolid-sites.com';
}
