#!/usr/bin/env bash

core=${SOLR_CORE#ds-}
core=${core//-/_}

if [[ -f /solr-conf/conf/solrcore.properties && -d /var/solr/data/${core}/conf ]]; then
  rm -r /var/solr/data/${core}/conf
  cp -a /solr-conf/conf /var/solr/data/${core}/
fi
