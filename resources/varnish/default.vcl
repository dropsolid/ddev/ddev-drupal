vcl 4.1;
import std;

backend default {
  .host = "web";
  .port = "80";
}

/*
 * Called at the beginning of a request, decide wheter to serve it.
 */
sub vcl_recv {
  // Trigger a redirect to the novarnish URL.
  if (std.port(server.ip) != 80) {
    return (synth(750));
  }

  // Protect against the HTTPOXY CGI vulnerability.
  unset req.http.Proxy;

  // Add an X-Forwarded-For header with the client IP address.
  if (req.restarts == 0) {
    if (req.http.X-Forwarded-For) {
      set req.http.X-Forwarded-For = req.http.X-Forwarded-For + ", " + client.ip;
    }
    else {
      set req.http.X-Forwarded-For = client.ip;
    }
  }

  // Store the original URL in a header
  set req.http.Original-Url = req.url;

  // Handle PURGE requests.
  if (req.method == "PURGE") {
    return (purge);
  }

  // Handle BAN requests.
  if (req.method == "BAN") {
    // Ban specific tags.
    if (req.http.X-Dropsolid-Purge-Tags) {
      ban("obj.http.X-Dropsolid-Purge-Tags ~ " + req.http.X-Dropsolid-Purge-Tags + " && obj.http.X-Dropsolid-Site == " + req.http.X-Dropsolid-Purge);
      return (synth(200, "Ban added."));
    }

    // Ban the whole site.
    if (req.http.X-Dropsolid-Purge-All) {
      ban("obj.http.X-Dropsolid-Site == " + req.http.X-Dropsolid-Purge);
      return (synth(200, "Ban added."));
    }

    return (synth(400, "Missing headers for a ban"));
  }

  // Only cache GET and HEAD requests.
  if (req.method != "GET" && req.method != "HEAD") {
    return (pass);
  }

  // Pass through some specific paths.
  if (req.url ~ "^/update\.php$" ||
      req.url ~ "^/admin(/|$)" ||
      req.url ~ "/system/files/" ||
      req.url ~ "^/flag/.*$" ||
      req.url ~ "/ajax/" ||
      req.url ~ "^/simplesaml(/|$)" ||
      req.url ~ "^/saml_login") {
    return (pass);
  }

  // Support websockets.
  if (req.http.Upgrade ~ "(?i)websocket") {
    return (pipe);
  }

  // Remove URL parameters to track effectiveness of online marketing campaigns.
  if (req.url ~ "(\?|&)(utm_[a-z]+|gclid|cx|ie|cof|siteurl|fbclid|__hs[a-z]+)=") {
    set req.url = regsuball(req.url, "(utm_[a-z]+|gclid|cx|ie|cof|siteurl|fbclid|__hs[a-z]+)=[-_A-z0-9+()%.]+&?", "");
    set req.url = regsub(req.url, "[?|&]+$", "");
  }

  # Strip hash and trailing ?.
  if (req.url ~ "\#") {
    set req.url = regsub(req.url, "\#.*$", "");
  }

  if (req.url ~ "\?$") {
    set req.url = regsub(req.url, "\?$", "");
  }

  // Pass all files that are almost certain to be static (or once generated static) to the backend.
  // We keep the cookies for those files because they won't give any overhead and might be needed for authentication.
  if (req.url ~ "(?i)\.(pdf|asc|dat|txt|doc|docx|xls|xlsx|ppt|pptx|tgz|csv|png|gif|jpeg|jpg|ico|swf|css|js|svg|ttf|eot|otf|woff|woff2)(\?.*)?$") {
    return (pass);
  }

  // Remove all cookies that Drupal doesn't need.
  if (req.http.Cookie) {
    set req.http.Cookie = ";" + req.http.Cookie;
    set req.http.Cookie = regsuball(req.http.Cookie, "; +", ";");

    // Set the X-Cookie-Ds-Vary header, which is a list of all DS_VARY_* cookies.
    set req.http.X-Cookie-Ds-Vary = regsuball(req.http.Cookie, ";(DS_VARY_[a-zA-Z0-9_]+)=", "; \1=");
    set req.http.X-Cookie-Ds-Vary = regsuball(req.http.X-Cookie-Ds-Vary, ";[^ ][^;]*", "");
    set req.http.X-Cookie-Ds-Vary = regsuball(req.http.X-Cookie-Ds-Vary, "^[; ]+|[; ]+$", "");

    if (req.http.X-Cookie-Ds-Vary == "") {
      unset req.http.X-Cookie-Ds-Vary;
    }

    // Set the X-Cookie-Ds-Bv header, which is a list of all DS_BV_* cookies.
    set req.http.X-Cookie-Ds-Bv = regsuball(req.http.Cookie, ";(DS_BV_[a-zA-Z0-9_]+)=", "; \1=");
    set req.http.X-Cookie-Ds-Bv = regsuball(req.http.X-Cookie-Ds-Bv, ";[^ ][^;]*", "");
    set req.http.X-Cookie-Ds-Bv = regsuball(req.http.X-Cookie-Ds-Bv, "^[; ]+|[; ]+$", "");

    if (req.http.X-Cookie-Ds-Bv == "") {
      unset req.http.X-Cookie-Ds-Bv;
    }

    // Remove all cookies except the session cookie, NO_CACHE cookies and SimpleSAML cookies.
    set req.http.Cookie = regsuball(req.http.Cookie, ";(S?SESS[a-z0-9]+|NO_CACHE|SimpleSAML[a-zA-Z0-9_]+)=", "; \1=");
    set req.http.Cookie = regsuball(req.http.Cookie, ";[^ ][^;]*", "");
    set req.http.Cookie = regsuball(req.http.Cookie, "^[; ]+|[; ]+$", "");

    // Don't cache if there are cookies left.
    if (req.http.Cookie != "") {
      return (pass);
    }

    // Remove the cookies.
    unset req.http.Cookie;
  }
}

/*
 * Add additional data to the cache hash.
 */
sub vcl_hash {
  if (req.http.X-Forwarded-Proto) {
    hash_data(req.http.X-Forwarded-Proto);
  }

  if (req.http.X-Cookie-Ds-Vary) {
    hash_data(req.http.X-Cookie-Ds-Vary);
  }
}

/*
 * Called when the response needs to be fetched from the backend.
 */
sub vcl_backend_fetch {
  // Add the DS_VARY_* cookies back to the cookie header.
  if (bereq.http.X-Cookie-Ds-Vary) {
    if (bereq.http.Cookie) {
      set bereq.http.Cookie = bereq.http.Cookie + ";" + bereq.http.X-Cookie-Ds-Vary;
    }
    else {
      set bereq.http.Cookie = bereq.http.X-Cookie-Ds-Vary;
    }

    unset bereq.http.X-Cookie-Ds-Vary;
  }

  // Add the DS_BV_* cookies back to the cookie header.
  if (bereq.http.X-Cookie-Ds-Bv) {
    if (bereq.http.Cookie) {
      set bereq.http.Cookie = bereq.http.Cookie + ";" + bereq.http.X-Cookie-Ds-Bv;
    }
    else {
      set bereq.http.Cookie = bereq.http.X-Cookie-Ds-Bv;
    }
  }
}

/*
 * Called after the response has been successfully retrieved from the backend.
 */
sub vcl_backend_response {
  // Set ban lurker friendly headers.
  set beresp.http.X-Url = bereq.url;
  set beresp.http.X-Host = bereq.http.Host;

  if (beresp.http.Vary) {
    // If cookies were send to the backend, store the Vary header without
    // "X-Cookie-Ds-Bv" so it can be restored later.
    if (bereq.http.Cookie) {
      set beresp.http.X-Vary = beresp.http.Vary;
      set beresp.http.X-Vary = regsuball(beresp.http.X-Vary, "(^|,[ ]*)X-Cookie-Ds-Bv([ ]*,|$)", "\1");
      set beresp.http.X-Vary = regsuball(beresp.http.X-Vary, "[, ]+$", "");

      if (beresp.http.X-Vary == "") {
        unset beresp.http.X-Vary;
      }
    }

    // Remove "Cookie" from the Vary header.
    set beresp.http.Vary = regsuball(beresp.http.Vary, "(^|,[ ]*)Cookie([ ]*,|$)", "\1");
    set beresp.http.Vary = regsuball(beresp.http.Vary, "[, ]+$", "");

    if (beresp.http.Vary == "") {
      unset beresp.http.Vary;
    }
  }

  // Don't cache items larger dan 2MB.
  if (std.integer(beresp.http.Content-Length, 0) > 2097152) {
    set beresp.uncacheable = true;
  }

  // Cache 404 and 50x for a short time.
  if (beresp.status == 404 || beresp.status == 500 || beresp.status == 502 || beresp.status == 503 || beresp.status == 504) {
    set beresp.ttl = 60s;
  }

  // Do not cache errors from image styles generation in Drupal.
  if ((beresp.status == 503 || beresp.status == 500) && bereq.url ~ "(?i)\.(png|gif|jpeg|jpg|ico)(\?itok=.*)?$") {
    set beresp.http.Cache-Control = "no-cache";
    set beresp.ttl = 0s;
  }

  // Enable streaming directly to backend for BigPipe responses.
  if (beresp.http.Surrogate-Control ~ "BigPipe/1.0") {
    set beresp.do_stream = true;
    set beresp.ttl = 0s;
  }

  // Allow items to remain in cache up to 6 hours past their cache expiration.
  set beresp.grace = 6h;

  // Re-add the marketing query params we stripped before on a redirect.
  if (beresp.status == 301 || beresp.status == 302 || beresp.status == 307 || beresp.status == 308) {
    set bereq.http.Marketing-Params = regsuball(bereq.http.Original-Url, "^.*(?=\?)|[?&](?!utm_[a-z]+=|gclid=|cx=|ie=|cof=|siteurl=|fbclid=|__hs[a-z]+=)[a-zA-Z_-]+(=[^&]*)?|\#.*$", "");

    if (bereq.http.Marketing-Params ~ "[?&]") {
      // Check if url already has query params
      if (beresp.http.Location !~ "(\?)") {
        set beresp.http.Location = regsub(beresp.http.Location, "\#.*$", "") + "?" + bereq.http.Marketing-Params + regsub(beresp.http.Location, "[^#]*", "");
      }
      else {
        set beresp.http.Location = regsub(beresp.http.Location, "\#.*$", "") + "&" + bereq.http.Marketing-Params + regsub(beresp.http.Location, "[^#]*", "");
      }

      // Strip occurrences of &? and ?& after removing params.
      set beresp.http.Location = regsub(beresp.http.Location, "(&\?|&&)", "&");
      set beresp.http.Location = regsub(beresp.http.Location, "(\?&|\?\?)", "?");

      // Trim trailing ? and &.
      set beresp.http.Location = regsub(beresp.http.Location, "(\?&|\?|&)$", "");
    }
  }
}

/*
 * Called before any object, except a vcl_synth result, is delivered to the client.
 */
sub vcl_deliver {
  // Remove the ban lurker friendly headers.
  unset resp.http.X-Url;
  unset resp.http.X-Host;

  // Restore the Vary header.
  if (resp.http.X-Vary) {
    set resp.http.Vary = resp.http.X-Vary;
    unset resp.http.X-Vary;
  }

  // Remove the Via header.
  unset resp.http.Via;

  if (obj.hits > 0) {
    set resp.http.Varnish-Result = "HIT (" + obj.hits + ")";
  }
  else {
    set resp.http.Varnish-Result = "MISS";
  }
}

/*
 * Called to return a synthetic response.
 */
sub vcl_synth {
  // Redirect to the novarnish URL on status code 750.
  if (resp.status == 750) {
    set resp.status = 301;
    set resp.http.Location = req.http.X-Forwarded-Proto + "://novarnish." + req.http.Host + req.url;
    set resp.reason = "Moved";
    return (deliver);
  }
}

/*
 * Called upon entering pipe mode.
 */
sub vcl_pipe {
  if (req.http.Upgrade) {
    set bereq.http.Upgrade = req.http.Upgrade;
  }

  return (pipe);
}
