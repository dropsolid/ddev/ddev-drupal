#!/usr/bin/env bash

# Leave if GrumPHP ain't installed.
export GRUMPHP=vendor/bin/grumphp

if [[ ! -x $GRUMPHP ]]; then
  GRUMPHP=dev-tools/vendor/bin/grumphp

  if [[ ! -x $GRUMPHP ]]; then
    exit 0
  fi
fi

# Fetch the git diff and format it as command input.
export DIFF=$(git -c diff.mnemonicprefix=false -c diff.noprefix=false --no-pager diff -r -p -m -M --full-index --no-color --staged | cat)

# Get the web container name.
container=$(cat .ddev/.ddev-docker-compose-full.yaml | grep ' container_name: ddev-.*-web$' | tr -s ' ' | cut -d ' ' -f 3)

# Disable xdebug.
[[ "$(ddev xdebug status)" =~ enabled ]]; xdebug_disabled=$?

if [[ $xdebug_disabled -eq 0 ]]; then
  ddev xdebug off > /dev/null
fi

# Run GrumPHP.
docker exec -e GRUMPHP -e DIFF -u "${USER}:${USER}" -i $container bash <<'COMMAND'
  (cd "./" && printf "%s\n" "$DIFF" | $GRUMPHP git:pre-commit --skip-success-output)
COMMAND

result=$?

# Enable xdebug again.
if [[ $xdebug_disabled -eq 0 ]]; then
  ddev xdebug > /dev/null
fi

exit $result
