# DDEV Drupal

This package provides a command that provisions various files (e.g. services/commands/...)
for developing a Drupal website in DDEV.


## Requirements

* [DDEV](https://ddev.com)


## Installation

Create a `.ddev/config.yaml` with following contents:

```yaml
# General.
name: ds-[name]
type: drupal
php_version: "8.3"
composer_version: 2
disable_settings_management: true
performance_mode: none

# Web.
webserver_type: apache-fpm
docroot: docroot

# Database.
database:
  type: mysql
  version: "5.7"

# Hooks.
hooks:
  pre-start:
    - exec-host: '[ -x vendor/bin/ddp ] && vendor/bin/ddp || exit 0'
```

**Important:**

* Set the name of the project, it's highly suggested to use `ds-[platform project name]`.
* Change the type if needed, but note that this package only supports Drupal 9 and newer.
* Unless you choose to disable the Drupal provisioning, settings management must be disabled.

Next add following repository and configuration to your `composer.json`:

```json
{
    "repositories": {
        "dropsolid-ddev": {
            "type": "composer",
            "url": "https://gitlab.com/api/v4/group/83070767/-/packages/composer/packages.json"
        }
    }
}
```

You also need to disable the [.htaccess scaffold](#user-content-htaccess) if Drupal Scaffold is used,
otherwise it will try to change the read-only file.

Now you can start your project and install this package by executing following commands:

```shell
ddev start

# Option 1 (if you don't commit vendor files).
ddev composer require --dev dropsolid/ddev-drupal

# Option 2 (if you commit vendor files).
ddev composer require --update-no-dev dropsolid/ddev-drupal

ddev restart
```

**Important:** It's required to restart your project after installing this package in order to activate the provisioned `docker-compose` configuration. The same applies when an update is installed.

If you are migrating from Launchpad please see the [additional tips/steps](#user-content-migrating-from-Launchpad).


## Configuration

The provisioning behavior can be configured by creating a `.ddev/ddp.ini` file, in which you can add following settings:

```ini
; Disable the provisioning of Drupal related files/folders/commands by setting this to false.
drupal=true

; Specify a Solr version (e.g. "9", see https://hub.docker.com/_/solr) to enable Solr.
solr=

; Disable the provisioning of Varnish by setting this to false.
varnish=true

; Disable the provisioning of Memcached by setting this to false.
memcached=true

; Enable the provisioning of Redis by setting this to true.
redis=false

; Disable adding the provisioned files to the .gitignore file by setting this to false.
gitignore=true

; Enable the provisioning of GrumPHP githooks by setting this to true.
grumphp=false
```

The values above are the defaults.


## Components

### Drupal

The Drupal provisioning mounts some specific platform files.


#### settings.php

The provisioned Drupal settings file (`.ddev/drupal/settings.php`) is almost identical to the one from Launchpad,
except for some cleanup and optimalizations. To override any settings create a local settings file at
`.dropsolid/scaffold/local/[your docroot]/sites/default/settings.php` or `etc/drupal/settings_local.php`
(when using the Drupal deploy flow).

**Important:**

* DDEV settings management must be disabled: `disable_settings_management: true`.
* By default the `settings.php` file is mounted readonly, you cannot change it from within the container.
  Add `drupal_settings_ro=false` to your configuration file (`.ddev/ddp.ini`) to mount it with write permissions.


#### .htaccess

If present, the `.htaccess` file is mounted from either `.dropsolid/scaffold/local/[your docroot]/.htaccess` or
`etc/drupal/.htaccess` (when using the Drupal deploy flow). Since this file is mounted readonly, you must either
disable the `.htaccess` scaffold or change the path accordingly in your `composer.json`:

```json
{
    "extra": {
        "drupal-scaffold": {
            "file-mapping": {
                "[web-root]/.htaccess": false
            }
        }
    }
}
```


#### Drush aliases

The `etc/drush` folder is mounted at `drush/sites` so your aliases can be found.


#### DDEV commands

##### deploy

The `deploy` command can be used to run the deploy/update script at `dropsolid/scaffold/local/deploy.sh`
or `bash/updates/update_local.sh` (when using the Drupal deploy flow).


##### potx

The `potx` command can be used as a shortcut to extract translations using potx.

```shell
# Extract translations from modules/custom/my_module
ddev potx my_module

# Extract translations from themes/custom/my_theme
ddev potx my_theme

# Extract translations from themes/contrib/contrib_theme
ddev potx themes/contrib/contrib_theme
```

A `pot` file named `general.pot` wil be created in your document root.
It's highly recomended to add this path to your `.gitignore` file to prevent comitting it accidentally.


### Solr

You can access Solr's UI via `http://[projectname].ddev.site:8983`. Use following details to configure your server:

* Solr connector: `Standard`
* HTTP protocol: `http`
* Solr host: `solr`
* Solr port: `8983`
* Solr path: `/`
* Default Solr collection: `[project name without ds- prefix]_local`

Export your config, extract it to `/etc/solr` and restart your project using `ddev restart` to reload the schema.


### Varnish

The included configuration is a slightly modified/cleaned copy of the configuration used by the Dropsolid Platform.

Keep following things in mind when Varnish is enabled:

* To access the web container directly you must prepend the hostname with `novarnish.`,
  this is import when accessing e.g. Mailpit. Varnish tries to redirect you whenever possible.
* In general all request cookies are removed, execept:
  * Any `(S)SESS...` session cookie
  * Any `SimpleSAML...` cookies
  * ANY `DS_VARY_...` cookies
  * The `NO_CACHE` cookie

Various commands are made available to manage and debug Varnish.


### Memcached

Memcached is available on port `11211` of the host named `memcached`.


### Redis

Redis is available on port `6379` of the host named `redis`.

The `redis-cli` command allows you to execute commands on the redis server.


### Gitignore

If enabled all provisioned files will be added to the `.gitignore` file.


### GrumPHP

If enabled both `commit-msg` and `pre-commit` githooks for GrumPHP will be provisioned.
These hooks will run GrumPHP if installed at `vendor/bin/grumphp` or `dev-tools/vendor/bin/grumphp`.


## Tips

### Manual provisioning

If you use the suggested DDEV configuration, or add its `pre-start` hook you don't have to provision manually.

If you prefer not to add this hook you can provision the provided files by executing the `vendor/bin/ddp`
command **before starting your environment**. Make sure you set following environment variables if the
default value ain't correct:

* `DDEV_APPROOT` (defaults to the current directory)
* `DDEV_DOCROOT` (defaults to `docroot`)


### Migrating from Launchpad

Just export (`drush sql-dump --result-file=db.sql`) your database in Launchpad and import it in
DDEV (`ddev import-db --file=docroot/db.sql`) and you'll be up and running in no time.

The default configuration allowes you to run both DDEV and Launchpad side by side, but note there are
(ignoring the obvious ones) some minor differences:

* The local settings file is included at the end of `settings.php`, so you can override everything.
* The default private files path has changed to `sites/default/files/private`
* Different Solr credentials

Old Rocketship projects also contained a `post-drupal-scaffold-cmd` script to modify the .htaccess file,
you should remove that (it's useless anyhow). Please see the [.htaccess section](#user-content-htaccess) for more details.


## Advanced topics

* Debugging and profiling: https://ddev.readthedocs.io/en/stable/users/debugging-profiling/step-debugging/
* Extending and customizing environments: https://ddev.readthedocs.io/en/stable/users/extend/customization-extendibility/
* Sharing a project: https://ddev.readthedocs.io/en/stable/users/topics/sharing/
* Linking 2 projects: https://ddev.readthedocs.io/en/stable/users/usage/faq/#can-different-projects-communicate-with-each-other
