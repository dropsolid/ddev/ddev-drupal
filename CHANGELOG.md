Changelog
=========

All notable changes to the project.


## 0.6.0 (2025-01-24):

### Added

* Support for DS_BV_* cookies

### Fixed

* Support for a none-scaffolded .htaccess file


## 0.5.0 (2024-09-13):

### Added

* Configuration option `drupal_settings_ro` to control whether `settings.php` is mounted read-only

### Changed

* Use Dropsolid VCL for Varnish


## 0.4.3 (2024-07-04):

### Fixed

* Compatibility with Drupal 9.3 and older


## 0.4.2 (2024-06-26):

### Added

* Included dev-tools/vendor/bin in $PATH

### Fixed

* Missing docroot on manual scaffold invocation
* .htaccess location check
* Hardcoded docroot in potx command


## 0.4.1 (2024-05-22):

### Fixed

* GrumPHP config check


## 0.4.0 (2024-05-17):

### Added

* Githooks to run GrumPHP on commit


## 0.3.0 (2024-05-16):

### Added

* Deploy command: `ddev deploy` runs your local deploy/update script

### Removed

* Separate `potx` config, the potx command is now managed by the `drupal` config


## 0.2.9 (2024-05-14):

### Fixed

* Missing private and tmp directories (default paths)


## 0.2.8 (2024-05-02):

### Fixed

* Error while parsing boolean config options


## 0.2.7 (2024-05-02):

### Added

* Support for other docroots paths

### Fixed

* Parsing boolean config options


## 0.2.6 (2024-04-30):

### Changed

* Reverted Drush aliases path


## 0.2.5 (2024-04-30):

### Added

* Support for the general deploy flow

### Fixed

* Missing / before paths added to .gitignore
