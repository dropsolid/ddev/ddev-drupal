<?php

/**
 * Provides the configuration.
 */
class Config {

  /**
   * The default configuration.
   */
  protected const DEFAULT_CONFIG = [
    'drupal' => TRUE,
    'drupal_settings_ro' => TRUE,
    'solr' => NULL,
    'varnish' => TRUE,
    'memcached' => TRUE,
    'redis' => FALSE,
    'gitignore' => TRUE,
    'grumphp' => FALSE,
  ];

  /**
   * The configuration.
   *
   * @var array
   */
  protected array $config;

  /**
   * Class constructor.
   *
   * @param string|null $file
   *   The configuration file to load.
   */
  public function __construct(string $file = '.ddev/ddp.ini') {
    $this->reset();

    if (isset($file) && file_exists($file)) {
      $this->loadFile($file);
    }
  }

  /**
   * Load a configuration file.
   *
   * @param string $file
   *   The file to load.
   */
  public function loadFile(string $file): void {
    // Note: we are using an ini file because the Yaml extension ain't
    // available by default in the PHP docker image.
    $ini = parse_ini_file($file);

    if ($ini === FALSE) {
      throw new RuntimeException("Failed reading configuration file '$file'");
    }

    foreach ($ini as $option => $value) {
      if (!array_key_exists($option, self::DEFAULT_CONFIG)) {
        throw new RuntimeException("Unkown configuration option '$option'");
      }

      // Boolean option.
      if (is_bool(self::DEFAULT_CONFIG[$option])) {
        if (in_array($value, ['yes', 'y', 'true', '1'], TRUE)) {
          $this->config[$option] = TRUE;
        }
        elseif (in_array($value, ['no', 'n', 'false', '0', ''], TRUE)) {
          $this->config[$option] = FALSE;
        }
        else {
          throw new RuntimeException("Invalid value for configuration option '$option'");
        }

        continue;
      }

      // Version number.
      if (!preg_match('/^\d+(\.\d+)*$/', $value)) {
        throw new RuntimeException("Invalid $option version number");
      }

      $this->config[$option] = $value;
    }
  }

  /**
   * Check if an option is configured.
   *
   * @param string $option
   *   The option name.
   *
   * @return bool
   *   TRUE if the option has been configured.
   */
  public function isset(string $option): bool {
    return isset($this->config[$option]);
  }

  /**
   * Get the value for a configuration option.
   *
   * @param string $option
   *   The option name.
   *
   * @return bool|string|null
   *   The option value, or NULL if not set.
   */
  public function get(string $option): bool|string|null {
    return $this->config[$option] ?? NULL;
  }

  /**
   * Load the default configuration.
   */
  public function reset(): void {
    $this->config = array_filter(self::DEFAULT_CONFIG, function ($value) {
      return isset($value);
    });
  }

}
