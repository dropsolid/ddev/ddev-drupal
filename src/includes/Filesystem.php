<?php

/**
 * Helper to deal with the filesystem.
 */
class Filesystem {

  /**
   * Check if a path is a file.
   *
   * @param string $path
   *   The path to check.
   *
   * @return bool
   *   TRUE if it's a file.
   */
  public function isFile(string $path): bool {
    return is_file($path);
  }

  /**
   * Check if a path is a directory.
   *
   * @param string $path
   *   The path to check.
   *
   * @return bool
   *   TRUE if it's a directory.
   */
  public function isDirectory(string $path): bool {
    return is_dir($path);
  }

  /**
   * Check if a path is writable.
   *
   * @param string $path
   *   The path to check.
   *
   * @return bool
   *   TRUE if it's writable.
   */
  public function isWritable(string $path): bool {
    return file_exists($path) && is_writable($path);
  }

  /**
   * Ensure a directory exists.
   *
   * @param string $path
   *   Path to the directory.
   * @param bool $writable
   *   Set to TRUE to also ensure it's writable.
   */
  public function ensureDirectory(string $path, bool $writable = FALSE): void {
    if ($this->isDirectory($path)) {
      if ($writable && !$this->isWritable($path)) {
        throw new RuntimeException("Directory '$path' is not a writable");
      }

      return;
    }

    if (file_exists($path)) {
      throw new RuntimeException("'$path' is not a directory");
    }

    if (!mkdir($path, 0755, TRUE)) {
      throw new RuntimeException("Directory '$path' could not be created");
    }
  }

  /**
   * List the contents of a directory.
   *
   * @param string $path
   *   The directory path.
   *
   * @return array
   *   The contents of the directory.
   */
  public function listDirectory(string $path): array {
    if (!$this->isDirectory($path)) {
      throw new RuntimeException("'$path' is not a directory");
    }

    $items = scandir($path);

    if ($items === FALSE) {
      throw new RuntimeException("Cannot read directory contents '$path'");
    }

    $items = array_diff($items, ['.', '..']);

    return array_values($items);
  }

  /**
   * Read a file.
   *
   * @param string $path
   *   The file path.
   *
   * @return string
   *   The file contents.
   */
  public function readFile(string $path): string {
    if (!$this->isFile($path)) {
      throw new RuntimeException("'$path' is not a file");
    }

    $contents = file_get_contents($path);

    if ($contents === FALSE) {
      throw new RuntimeException("Failed to read '$path'");
    }

    return $contents;
  }

  /**
   * Write to a file.
   *
   * @param string $path
   *   The file path.
   * @param string $contents
   *   The file contents.
   */
  public function writeFile(string $path, string $contents): void {
    if (!file_put_contents($path, $contents)) {
      throw new RuntimeException("Failed to write '$path'");
    }
  }

  /**
   * Remove a file.
   *
   * @param string $path
   *   The file path.
   */
  public function removeFile(string $path): void {
    if ($this->isFile($path)) {
      if (!unlink($path)) {
        throw new RuntimeException("'$path' could not be removed");
      }
    }
    elseif ($this->isDirectory($path)) {
      throw new RuntimeException("'$path' is not a file");
    }
  }

  /**
   * Copy a file.
   *
   * @param string $from
   *   The source file path.
   * @param string $to
   *   The destination file path.
   * @param int $chmod
   *   The chmod to apply.
   */
  public function copyFile(string $from, string $to, ?int $chmod = NULL): void {
    if (!$this->isFile($from)) {
      throw new RuntimeException("'$from' is not a file");
    }

    if (!copy($from, $to)) {
      throw new RuntimeException("Failed to copy '$from' to '$to'");
    }

    if ($chmod !== NULL && !chmod($to, $chmod)) {
      throw new RuntimeException("Failed to apply chmod $chmod on '$to'");
    }
  }

}
