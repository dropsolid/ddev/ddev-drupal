<?php

require_once 'Filesystem.php';

/**
 * Helper to copy files.
 */
class FileCopier {

  /**
   * The source root.
   *
   * @var string
   */
  protected string $sourceRoot;

  /**
   * The destination root.
   *
   * @var string
   */
  protected string $destinationRoot;

  /**
   * List of copied files.
   *
   * @var string[]
   */
  protected array $copiedFiles = [];

  /**
   * The filesystem.
   *
   * @var \Filesystem
   */
  protected Filesystem $fs;

  /**
   * Class constructor.
   *
   * @param string $source_root
   *   The source root path.
   * @param string $destination_root
   *   The destination root path.
   */
  public function __construct(string $source_root, string $destination_root) {
    $this->sourceRoot = $this->prepareRoot($source_root);
    $this->destinationRoot = $this->prepareRoot($destination_root);
    $this->fs = new Filesystem();
  }

  /**
   * Copy a file or directory.
   *
   * @param string $from
   *   The source path.
   * @param string|null $to
   *   The destination path, leave empty to use the same path as $from.
   */
  public function copyDirectory(string $from, string $to = NULL): void {
    if ($to === NULL) {
      $to = $from;
    }

    // Prepare the source and destination paths.
    $from = trim($from, '/');
    $to = trim($to, '/');

    // Read the directory contents.
    $from_full = $this->sourceRoot . $from;
    $items = $this->fs->listDirectory($from_full);

    foreach ($items as $item) {
      $item_from = $from . '/' . $item;
      $item_to = $to . '/' . $item;

      if ($this->fs->isDirectory($from_full . '/' . $item)) {
        $this->copyDirectory($item_from, $item_to);
      }
      else {
        $this->copyFile($item_from, $item_to);
      }
    }
  }

  /**
   * Copy a file and replace any variables.
   *
   * @param string $from
   *   The source file path.
   * @param string|null $to
   *   The destination path, leave empty to use the same path as $from.
   * @param array $variables
   *   Associative array of variables to be replaced.
   */
  public function copyFile(string $from, string $to = NULL, array $variables = []): void {
    if ($to === NULL) {
      $to = $from;
    }

    // Prepare the source and destination paths.
    $from = ltrim($from, '/');
    $from_full = $this->sourceRoot . $from;
    $to = ltrim($to, '/');
    $to_full = $this->destinationRoot . $to;

    // Ensure the directory is writable.
    if ($to !== $to_full) {
      $this->fs->ensureDirectory(dirname($to_full), TRUE);
    }
    elseif (!$this->fs->isWritable('.')) {
      throw new RuntimeException("The current directory is not a writable");
    }

    // Copy the file.
    $this->fs->copyFile($from_full, $to_full);
    $this->copiedFiles[] = $to;

    // Leave if there are no variables to be replaced.
    if (empty($variables)) {
      return;
    }

    // Prepare the tokens.
    $tokens = array_keys($variables);
    array_walk($tokens, function(string &$token) {
      $token = '[[DDP_' . mb_strtoupper($token) . ']]';
    });

    // Read the file, replace the tokens and save it again.
    $contents = $this->fs->readFile($to_full);
    $contents = str_replace($tokens, $variables, $contents);
    $this->fs->writeFile($to_full, $contents);
  }

  /**
   * Get a list of the copied files.
   *
   * @param bool $include_destionation_root
   *   Set to TRUE to include the destination root.
   *
   * @return string[]
   *   The copied file paths.
   */
  public function getCopiedFiles(bool $include_destionation_root = FALSE): array {
    if (!$include_destionation_root) {
      return $this->copiedFiles;
    }

    $files = $this->copiedFiles;
    array_walk($files, function (string &$path) {
      $path = $this->destinationRoot . $path;
    });

    return $files;
  }

  /**
   * Reste teh  the list of copied files.
   *
   * @return void
   */
  public function resetCopiedFiles(): void {
    $this->copiedFiles = [];
  }

  /**
   * Prepare the path to a root directory.
   *
   * @param string $root
   *   The root directory path.
   *
   * @return string
   *   The prepared root directory path.
   */
  protected function prepareRoot(string $root): string {
    if ($root === '' || $root === './') {
      return '';
    }

    return rtrim($root, '/') . '/';
  }

}
