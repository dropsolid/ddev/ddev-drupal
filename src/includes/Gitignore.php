<?php

require_once 'Filesystem.php';

/**
 * Helper to manage the gitignore file.
 */
class Gitignore {

  /**
   * Start and end token.
   */
  protected const START_TOKEN = '### Begin Dropsolid DDEV Provisioning.';
  protected const END_TOKEN = '### End Dropsolid DDEV Provisioning.';

  /**
   * The .gitignore path.
   *
   * @var string
   */
  protected string $path;

  /**
   * The linebreak sequence.
   *
   * @var string
   */
  protected string $linebreak;

  /**
   * The filesystem.
   *
   * @var \Filesystem
   */
  protected Filesystem $fs;

  /**
   * Class constructor.
   *
   * @param string $path
   *   The .gitignore path.
   * @param string $linebreak
   *   The linebreak sequence.
   */
  public function __construct(string $path = '.gitignore', string $linebreak = "\n") {
    $this->path = $path;
    $this->linebreak = $linebreak;
    $this->fs = new Filesystem();
  }

  /**
   * Set the files to ignore.
   *
   * @param array $files
   *   A list of file paths to ignore.
   * @param string $prefix
   *   A prefix to add before each path.
   */
  public function setFiles(array $files, string $prefix = '/'): void {
    if (empty($files)) {
      $this->removeFiles();
      return;
    }

    $files = array_unique($files);
    sort($files);

    $lines = self::START_TOKEN . $this->linebreak;
    $lines .= $prefix . implode($this->linebreak . $prefix, $files) . $this->linebreak;
    $lines .= self::END_TOKEN;

    $this->updateFile($lines);
  }

  /**
   * Remove all files currently being ignored.
   */
  public function removeFiles(): void {
    if (!$this->fs->isFile($this->path)) {
      return;
    }

    $this->updateFile('');
  }

  /**
   * Update the gitignore file.
   *
   * @param string $lines
   *   The new gitignore lines.
   */
  protected function updateFile(string $lines): void {
    $contents = '';

    if ($this->fs->isFile($this->path)) {
      $contents = $this->fs->readFile($this->path);
    }
    elseif (trim($lines) === '') {
      return;
    }

    $pattern = '/^' . preg_quote(self::START_TOKEN, '/');
    $pattern .= '[\r\n]+(.+?[\r\n]+)?';
    $pattern .= preg_quote(self::END_TOKEN, '/') . '[ \t]*$/sum';
    $count = 0;
    $contents_new = preg_replace($pattern, $lines, $contents, 1, $count);

    if ($count === 0) {
      $contents_new = rtrim($contents_new);

      if ($contents_new !== '') {
        $contents_new .= $this->linebreak . $this->linebreak;
      }

      $contents_new .= $lines . $this->linebreak;
    }

    if (trim($contents_new) === '') {
      $this->fs->removeFile($this->path);
    }
    elseif ($contents_new !== $contents) {
      $this->fs->writeFile($this->path, $contents_new);
    }
  }

}
