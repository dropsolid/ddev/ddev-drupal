<?php

/**
 * @file
 * Provision the configuration files.
 */

require_once 'includes/Config.php';
require_once 'includes/FileCopier.php';
require_once 'includes/Filesystem.php';
require_once 'includes/Gitignore.php';

// Create the global classes.
$config = new Config();
$fc = new FileCopier('../package/resources', '.ddev');
$fs = new Filesystem();

// Get the docroot.
if (empty($argv[1])) {
  throw new \RuntimeException("Docroot not specified");
}

$docroot = $argv[1];

// Files to ignore.
$gitignore_files = [];

// Drupal.
if ($config->get('drupal')) {
  $gitignore_files[] = 'drush';

  $htaccess_file = NULL;
  foreach ([".dropsolid/scaffold/local/$docroot", 'etc/drupal'] as $path) {
    $path .= '/.htaccess';

    if ($fs->isFile($path)) {
      $htaccess_file = $path;
      break;
    }
  }

  $fc->copyFile('docker-compose.drupal.yaml', NULL, [
    'SETTINGS_OPTIONS' => $config->get('drupal_settings_ro') ? ':ro' : '',
    'HTACCESS_FILE' => $htaccess_file ?? "$docroot/.htaccess",
  ]);
  $fc->copyFile('commands/web/deploy');
  $fc->copyFile('commands/web/potx');
  $fc->copyFile('drupal/settings.php', NULL, [
    'DOCROOT' => $docroot,
  ]);
  $fs->ensureDirectory('drush/sites');
  $fs->ensureDirectory('etc/drush');
  $fs->ensureDirectory($docroot, TRUE);
  $fs->ensureDirectory($docroot . '/sites/default/files/private', TRUE);
  $fs->ensureDirectory($docroot . '/sites/default/files/tmp', TRUE);

  if ($htaccess_file !== NULL) {
    $fs->writeFile("$docroot/.htaccess", "# File not used, see $htaccess_file\n");
    $gitignore_files[] = "$docroot/.htaccess";
  }
}

// Solr.
if ($config->isset('solr')) {
  $fc->copyFile('docker-compose.solr.yaml', NULL, [
    'SOLR_VERSION' => $config->get('solr')
  ]);
  $fc->copyDirectory('solr');
  $fs->ensureDirectory('etc/solr');
}

// Varnish.
if ($config->get('varnish')) {
  $fc->copyFile('docker-compose.varnish.yaml');
  $fc->copyDirectory('varnish');
  $fc->copyDirectory('commands/varnish');
}

// Memcached.
if ($config->get('memcached')) {
  $fc->copyFile('docker-compose.memcached.yaml');
}

// Redis.
if ($config->get('redis')) {
  $fc->copyFile('docker-compose.redis.yaml');
  $fc->copyDirectory('redis');
  $fc->copyDirectory('commands/redis');
}

// Custom dev tools path.
if ($fs->isDirectory('dev-tools/vendor/bin')) {
  $fc->copyFile('homeadditions/.bashrc.d/dev-tools-path.sh');
}

// Update the gitignore file.
if (!$config->get('gitignore')) {
  $gitignore_files = NULL;
}
else {
  $gitignore_files = array_merge(
    $gitignore_files,
    $fc->getCopiedFiles(TRUE)
  );
}

$gitignore = new Gitignore();

if (!empty($gitignore_files)) {
  $gitignore->setFiles($gitignore_files);
}
else {
  $gitignore->removeFiles();
}

// GrumPHP.
if ($config->get('grumphp')) {
  $fs->copyFile('../package/resources/githooks/commit-msg', '.git/hooks/commit-msg', 0755);
  $fs->copyFile('../package/resources/githooks/pre-commit', '.git/hooks/pre-commit', 0755);
}
