#!/usr/bin/env bash

#######################################################################################
#                                                                                     #
#                             Dropsolid DDEV Provisioning                             #
#                                                                                     #
# Issues: https://redmine.dropsolid.com/projects/internal-ddev                        #
# Repository: https://gitlab.internal.dropsolid.com/dropsolid-agency/ddev/ddev-drupal #
#                                                                                     #
#######################################################################################

# Help message.
if [[ "$1" == '--help' || "$1" == '-h' ]]; then
  yellow=$(tput setaf 3)
  cyan=$(tput setaf 6)
  normal=$(tput sgr0)

  printf '%s\n\n' "${yellow}Dropsolid DDEV Provisioning${normal}"

  printf '%s\n' 'This command provisions various files (e.g. services/commands/...) for DDEV.'
  printf '%s\n' "The resourcesing can be configured by creating ${cyan}.ddev/ddp.ini${normal}"
  printf '%s\n\n' 'file with the component specific configuration.'

  printf '%s\n' "See the ${cyan}README${normal} in ${cyan}dropsolid/ddev-drupal${normal} for more details."
  exit
fi

# Get the user and group ID.
uid=$(id -u)
gid=$(id -g)

# Get the project directory.
current_dir="$(pwd)"
project_dir="${DDEV_APPROOT:-$current_dir}"

# Get the package directory.
package_dir="$(cd -- "$(dirname "$0")"; pwd -P)"
package_dir="$(dirname "$package_dir")"

# Provision via a docker container to have a stable environment.
docker run -i --rm \
  --name ddp \
  --mount type=bind,source="$project_dir",target=/usr/src/project \
  --mount type=bind,source="$package_dir",target=/usr/src/package \
  -u $uid:$gid \
  -w /usr/src/project \
  php:8.2-cli \
  php ../package/src/ddp.php "${DDEV_DOCROOT:-docroot}"
